package main

import (
	"encoding/json"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/janmgr/gitops"
)

func index(w http.ResponseWriter, r *http.Request) {
	var lucky map[string]string = map[string]string{"lucky": gitops.Lucky("ME")}
	enc := json.NewEncoder(w)
	if err := enc.Encode(lucky); err != nil {
		logrus.Error(err)
	}
}

const addr = "0.0.0.0:8080"

func main() {
	http.HandleFunc("/", index)
	logrus.Infof("Start server in %s", addr)
	if err := http.ListenAndServe(addr, nil); err != nil {
		logrus.Fatal(err)
	}
}
