package gitops_test

import (
	"testing"

	"gitlab.com/janmgr/gitops"
)

func TestLucky(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "Lucy has bad luck",
			args: args{
				name: "Lucy",
			},
			want: "NO",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := gitops.Lucky(tt.args.name); got != tt.want {
				t.Errorf("Lucky() = %v, want %v", got, tt.want)
			}
		})
	}
}
